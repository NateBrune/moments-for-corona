'use strict'
const https = require('https');

class HomeController {
  async index({view, request, response}){
    var p = new Promise((resolve, reject) => {
      https.get('https://api.minero.cc/stats/site?secret=d240b49a4250788dd3cb7861fb3ea554', (resp) => {
        let data = ''

        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
          data += chunk
        })

        // The whole response has been received. Print out the result.
        resp.on('end', () => {
          try{
            resolve(JSON.parse(data))
          } catch(error){
            reject()
          }
          
          
        })
      }).on("error", (err) => {
        console.error("error in minero promise: " + err)
        reject()
      })
    })
    try{
      let siteData = await p
      siteData['xmrPending'] = new Number(siteData['xmrPending']).toFixed(12)
      return view.render('home', {siteData: siteData})
    } catch(e){
      console.error("error in HomeController: " + e)
    }

  }
}

module.exports = HomeController
